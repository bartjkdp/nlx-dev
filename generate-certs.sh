#!/bin/sh

set -e # exit on error
cd certs/

for certHost in "directory-inspection-api" "directory-registration-api" "directory-monitor" "config-api" "management-api" "outway" "inway" "nlxctl"; do
    csrFilename="${certHost}-csr.json"
    echo '{"hosts": ["'${certHost}'.test"], "key": {"algo": "rsa", "size": 2048}, "CN": "'${certHost}'.test", "names": [{"O": "NLX", "OU": "NLX"}]}' > "${csrFilename}"

    ## Generate and sign cert using remote CA (cfssl server)
    cfssl gencert -ca=ca.pem -ca-key=ca-key.pem "${csrFilename}" | cfssljson -bare "${certHost}"
done
