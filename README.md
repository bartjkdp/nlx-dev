# NLX dev
This repository contains some utilities of @bartjkdp for running and developing NLX.

# Ports
- 6000: postgres
- 6001: etcd
- 6010: directory-inspection-api (HTTP)
- 6011: directory-inspection-api (gRPC)
- 6012: directory-registration-api (gRPC)
- 6013: config-api (gRPC)
- 6014: outway (HTTP)
- 6015: inway (HTTP)
- 6016: management-api (HTTP)

# Important URLs
- http://directory-inspection-api.test:6010/directory/list-services
- http://outway.test:6014/NLX/PostmanEcho/get\?id\=1

# Initialize the setup

```bash
./initialize-ca.sh
./generate-certs.sh
```

Then bring the environment up with:

```bash
docker-compose up
```

# Running nlxctl
Initially configure nlxctl with:

```bash
go run ~/dev/nlx/nlxctl/main.go init -a config-api.test:6013 --ca ~/dev/nlx-dev/certs/ca.pem --cert ~/dev/nlx-dev/certs/nlxctl.pem --key ~/dev/nlx-dev/certs/nlxctl-key.pem
```

This creates a configuration in `~/.nlxctl-config.yaml`.

Then use nlxctl:

```bash
go run ~/dev/nlx/nlxctl/main.go service create -c ~/dev/nlx/config-api/config/service.json
go run ~/dev/nlx/nlxctl/main.go service list
```
