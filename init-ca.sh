#!/bin/sh

cd certs/

echo '{"CN": "NLX", "key": {"algo": "rsa", "size": 2048}, "names": [{"O": "NLX", "OU": "NLX"}]}' > ca-csr.json
cfssl gencert -initca ca-csr.json | cfssljson -bare ca
